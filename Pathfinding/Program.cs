﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// GL&HF Próba stworzenia Pathfinding(wspomagając się pseudokodem algorytmu Djikstry i A*)
// Autor: Patryk Gusarzewski
/*To do
- pierwsza funkcja dla startowego punktu albo to jakoś sprytnie obejść
- Zrobić działające AllDroga i Droga (dodawanie i usuwanie)
- Zrobić funkcje skracającą drogę jeśli użyty później punkt był sąsiadem wcześniejszego innego punktu np. A3->A5->A1->A8 -----> A3->A5->A8
*/
// Dobra nie działa to, bo coś się zjebało i wczytuje wszystkie elementy jako już pominięte(one wszystkie są już w tablicy Pominiete w jakiś sposob)
public class Punkt
{
    public int x; // X i Y na mapie
    public int y;
    public int wartosc; //W liscie drogi
    public Punkt()
    { }
    public Punkt(Punkt c)
    {
        this.x = c.x;
        this.y = c.y;
        this.wartosc = c.wartosc;
    }
    public Punkt(int y, int x)
    {
        this.x = x;
        this.y = y;
    }
    public void nowe(Punkt nowy)
    {
        this.x = nowy.x;
        this.y = nowy.y;
        this.wartosc = nowy.wartosc;
    }
    public void set_x(int x)
    {
        this.x = x;
    }
    public void set_y(int y)
    {
        this.y = y;
    }
    public void set_w(int war)
    {
        wartosc = war;
    }
}

public class Finder
{
    public int[,] FMapa;
    public Punkt Start;
    public Punkt Cel;
    public Punkt Obecny;
    public Punkt[] AllDroga; // Spis WSZYSTKICH odwiedzonych punktów
    public Punkt[] Droga;  // Poprawna droga
    public Punkt[] Pomijane;
    public int alldroga_licznik = 0;
    public int pomijane_licznik = 0;
    public int limit = 0;
    public int droga_licznik = 0;

    public Finder(int[,] Fmapa, Punkt Start, Punkt Cel)
    {
        this.FMapa = Fmapa;
        this.Start = Start;
        this.Cel = Cel;
        this.AllDroga = new Punkt[25]; // Spis WSZYSTKICH odwiedzonych punktów
        this.Droga= new Punkt[25];  // Poprawna droga
        this.Pomijane = new Punkt[1000];
}
    public Punkt[] Optymalizacja(Punkt[] Sciezka,int[,] OMapa)
    {
        Punkt Help = new Punkt();
        int licznik = droga_licznik;

        for (int od_konca = licznik; od_konca > 0; od_konca--)
        {
            Punkt Sprawdzajka = Help;
            Sprawdzajka.nowe(Sciezka[od_konca]);
            for (int j = -1; j <= 1; ++j)
            {
                for (int i = -1; i <= 1; ++i)
                {

                    Sciezka[droga_licznik] = Help;
                }
            }
        }
                return Sciezka;
    }



    public void Cofnij(Punkt aktualny)
    {
        Punkt Help = new Punkt();
        Pomijane[pomijane_licznik] = Help;
        Pomijane[pomijane_licznik].nowe(Droga[droga_licznik]);
        Droga[droga_licznik] = Help;
        droga_licznik--;
        pomijane_licznik++;
        try {
            Console.WriteLine("Cofam!");
            Best_sasiad(Droga[droga_licznik]);
            }
        catch (IndexOutOfRangeException) {
            Console.WriteLine("Z tego punktu nie ma drogi wyjscia!");       //Jeśli nie da się przejsc do wyjścia
        }
    }




    public Punkt[] Best_sasiad(Punkt aktualny)
    {
            AllDroga[alldroga_licznik] = aktualny;
            AllDroga[alldroga_licznik].nowe(aktualny);
            Console.WriteLine("y-" + AllDroga[alldroga_licznik].y + " x-" + AllDroga[alldroga_licznik].x + " wartosc-" + AllDroga[alldroga_licznik].wartosc);
            alldroga_licznik++;
            Punkt Sprw = new Punkt();
            Sprw.nowe(aktualny);
            Punkt Best = new Punkt();
            Punkt Help = new Punkt();
            Best.set_w(99);
            Console.WriteLine(Best.wartosc);
            bool czy_znaleziono = false;
            bool czy_bylo;
            int pomagajkax, pomagajkay;     //Do sprawdzenia
            for (int j = -1; j <= 1; ++j)
            {
                for (int i = -1; i <= 1; ++i)
                {
                    czy_bylo = false;
                    Console.WriteLine("Nasze j i wynosi" + aktualny.y + j + " " + aktualny.x + i);
                    if (aktualny.x + i == Cel.x && aktualny.y + j == Cel.y)          //Sprawdz czy ten sasiad to twoj cel
                    {
                        Droga[droga_licznik].nowe(aktualny);
                        droga_licznik++;
                    Console.WriteLine("Naszego celu j i wynosi" + Cel.y + " " + Cel.x);
                    Droga[droga_licznik] = Help;
                    Droga[droga_licznik].nowe(Cel);
                        return Droga;
                    }

                    if (i == 0 && j == 0) { Console.WriteLine("Nie sprawdzam sam siebie"); continue; }    //Pomija punkt w którym jesteśmy

                for (int sprawdz = 0; sprawdz < pomijane_licznik; sprawdz++)
                {
                    pomagajkax = aktualny.x + i;
                    pomagajkay = aktualny.y + j;
                    if (Pomijane[sprawdz].x == pomagajkax && Pomijane[sprawdz].y == pomagajkay)
                    {
                        czy_bylo = true;
                        break;
                    }
                }
                if (czy_bylo) { continue; }

                for (int sprawdz2 = 0; sprawdz2 < alldroga_licznik; sprawdz2++)
                {
                    pomagajkax = aktualny.x + i;
                    pomagajkay = aktualny.y + j;
                    if (AllDroga[sprawdz2].x == pomagajkax && AllDroga[sprawdz2].y == pomagajkay) { Console.WriteLine("Allmapa pomijam " + pomagajkay + pomagajkax); czy_bylo = true; break; }
                }
                if (czy_bylo) { continue; }

                for (int sprawdz3 = 0; sprawdz3 <= droga_licznik; sprawdz3++)
                {
                    
                    pomagajkax = aktualny.x + i;
                    pomagajkay = aktualny.y + j;
                    if (Droga[sprawdz3].x == pomagajkax && Droga[sprawdz3].y == pomagajkay) { Console.WriteLine("Droga pomijam" + pomagajkay + pomagajkax); czy_bylo = true; break; }
                }
                if (czy_bylo) { continue; }

                try
                    {
                        Sprw.set_w(FMapa[aktualny.y + j, aktualny.x + i]);                                                  //Wrzucam nowy punkt do sprawdzenia(a try sprawdza czy nie jest spoza Mapy)
                        Sprw.set_x(aktualny.x + i);
                        Sprw.set_y(aktualny.y + j);
                    }
                    catch (IndexOutOfRangeException)                                                                // Jesli wyjde poza tablicę to zapamiętaj by więcej tam nie wchodzić
                    {
                    Pomijane[pomijane_licznik] = Help;
                    Pomijane[pomijane_licznik].nowe(Sprw);
                    pomijane_licznik++;
                        Console.WriteLine("Poza Mapą");
                        continue;
                    }
                    Console.WriteLine("Best to " + Best.wartosc);
                    if (Sprw.wartosc > 100)                                                                         // Jezeli jest to sciana dodaj do pomijanych
                    {
                        Pomijane[pomijane_licznik] = Help;
                        Pomijane[pomijane_licznik].nowe(Sprw);
                        pomijane_licznik++;
                        Console.WriteLine("To jest sciana, bede ja omijal");
                        continue;
                    }
                    Console.WriteLine("Porownanie: " + Sprw.wartosc+"kontra " + Best.wartosc);     //Ustawianie BEST
                    if (Sprw.wartosc < Best.wartosc)
                    {
                        Console.WriteLine("Znalazlem best " + Sprw.wartosc + " o lokalizacji y="+Sprw.y+" x="+Sprw.x);
                        Best.nowe(Sprw);
                        czy_znaleziono = true;
                    }
                }
            }
            if (czy_znaleziono) {
                droga_licznik++;
            try
            {
                Droga[droga_licznik] = Best;
                Droga[droga_licznik].nowe(Best);
                Console.WriteLine("Przechodze do " + Droga[droga_licznik].wartosc + " o lokalizacji y=" + Droga[droga_licznik].y + " x=" + Droga[droga_licznik].x);
            }
            catch (IndexOutOfRangeException) { return Droga; }
                Console.WriteLine(Best.wartosc);
                Best_sasiad(Best);
            }
            else {
                limit++;
                if (limit > 50)
                {
                    return this.Droga;
                }
                Cofnij(aktualny);
            }
            return Droga;
    }
}

namespace Pathfinding
{
    class Program
    {
        static void Main(string[] args)
        {
            const int X = 100; // Ściana/Woda/Lawa czy jakaś inna przeszkoda
            int[,] Mapa = new int[,]{
                {5,     4,        1,      3,   3},
                {X+1,   3,        X+2,    2,   4},   // Najlepszym wynikiem powinna być droga: 1,2,4,7,9,13,16,17,19
                {X+3,   X+4,      X+5,    1,   5},
                {5,     2,        X+6,    3,   2},
                {4,     3,        8,      6,   5}};
            Console.WriteLine("Podaj skad dokad");
            int x11 = Convert.ToInt32(Console.ReadLine());
            int x12 = Convert.ToInt32(Console.ReadLine());
            int x21 = Convert.ToInt32(Console.ReadLine());
            int x22 = Convert.ToInt32(Console.ReadLine());
            Punkt x1 = new Punkt(x11,x12);
            x1.wartosc = Mapa[x11, x12];
            Punkt x2 = new Punkt(x21, x22);
            x2.wartosc = Mapa[x21, x22];
            Console.WriteLine("  X->   0 1 2 3 4");
            Console.WriteLine("      0|5 4 1 3 3|");
            Console.WriteLine("      1|X 2 X 2 4|");
            Console.WriteLine("  Y-> 2|X X X 7 5|");
            Console.WriteLine("      3|5 2 X 3 2|");
            Console.WriteLine("      4|4 3 8 6 5|");
            Console.WriteLine(Mapa.GetUpperBound(0)); 
            Finder funkcja = new Finder(Mapa,x1,x2);
            Punkt[] Nasza_droga = new Punkt[25];
            funkcja.Droga[0] = x1;
            Nasza_droga = funkcja.Best_sasiad(x1);
            Console.WriteLine("Nasza droga Y X");
            for (int wypisz = 0; wypisz <= funkcja.droga_licznik; wypisz++)
            {
                Console.WriteLine("Punkt " + Nasza_droga[wypisz].y + " " + Nasza_droga[wypisz].x + " o wartosci " + Nasza_droga[wypisz].wartosc);
            }
            Console.ReadKey();
          
        }
    }
}
// Pomysły
// Przy liście punktów sąsiednich będę miał dużo j*bania, ale nawet nie wiem czy to będzie czytelniejsze dla programu i przede wszystkim, dla mnie :/
// 1. Zrobić algorytm zachłanny 2. Później myślimy dalej 3. Wygrać NAI (doskonały plan)
